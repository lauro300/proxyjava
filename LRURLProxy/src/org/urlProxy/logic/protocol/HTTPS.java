/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.logic.protocol;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.urlProxy.logic.message.Message;
import static org.urlProxy.logic.protocol.AbstractProtocol.encoder;
import org.urlProxy.session.Session;

/**
 *
 * @author jose_castaneda
 */
public class HTTPS extends AbstractProtocol{
    
    public HTTPS(){
        super(TYPE_PROTOCOL.HTTPS);
    }
    @Override
    protected void executeProtocol(Message message, Session session, ByteBuffer buffer) {
        try {
            session.getClient().write(encoder.encode(CharBuffer.wrap("HTTP/1.1 200 Connection established\r\nProxy-agent: RPS-Proxy/1.0\r\n\r\n")));
        } catch (IOException ex) {
            Logger.getLogger(HTTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
