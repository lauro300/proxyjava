/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.logic.protocol;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.Charset;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.urlProxy.logic.message.Message;
import org.urlProxy.session.Session;

/**
 *
 * @author jose_castaneda
 */
public abstract class AbstractProtocol {
    public static final CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();
    public enum TYPE_PROTOCOL{HTTP, HTTPS};
    private TYPE_PROTOCOL type;
    private Optional<AbstractProtocol> next;
    
    protected AbstractProtocol(TYPE_PROTOCOL type){
        this.type = type;
    }
    
    public void setNextProtocol(AbstractProtocol protocol){
        next = Optional.of(protocol);
    }
    
    public void applicateProtocol(Message message, Session session, ByteBuffer buffer) {
        if(this.type==message.getProtocol()){
            executeProtocol(message, session, buffer);
            return;
        }
        next.ifPresent((nextPr)->{nextPr.executeProtocol(message, session, buffer);});
    }
    
    protected void write(OutputStream out, byte[] message) {
        try {
            out.write(message);
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(AbstractProtocol.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected int read(InputStream in, byte[] message) {
        int read = 0;
        try {
            read = in.read(message);
        } catch (IOException ex) {
            Logger.getLogger(AbstractProtocol.class.getName()).log(Level.SEVERE, null, ex);
        }
        return read;
    }
    
    protected String getUserAgent(){
        return "User-Agent: URLProxy";
    }
    
    abstract protected void executeProtocol(Message message, Session session, ByteBuffer buffer);
}
