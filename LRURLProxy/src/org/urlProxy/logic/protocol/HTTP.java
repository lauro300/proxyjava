/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.logic.protocol;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.urlProxy.Proxy;
import org.urlProxy.logic.message.Message;
import org.urlProxy.session.Session;

/**
 *
 * @author jose_castaneda
 */
public class HTTP extends AbstractProtocol{
    byte buffer[] = new byte[1024];
    public HTTP(){
        super(TYPE_PROTOCOL.HTTP);
    }
    @Override
    protected void executeProtocol(Message message, Session session, ByteBuffer buffer) {
        StringBuilder head = new StringBuilder();
        head.append(message.getURL()).append("\n")
        .append("Host: ").append(message.getHost()).append("\n")
        .append(message.getTailMessage())
        .append("\r\n");
        if(Proxy.verbose){
            Logger.getLogger(HTTP.class.getName()).log(Level.INFO, "Request Connection:{0}",head.toString());
        }
        session.getRemoteResource().ifPresent(socket->{
            try {
                socket.write(encoder.encode(CharBuffer.wrap(head.toString())));
            } catch (IOException ex) {
                Logger.getLogger(HTTP.class.getName()).log(Level.SEVERE, "Request Connection", ex);
            }
        });
    }
    
}
