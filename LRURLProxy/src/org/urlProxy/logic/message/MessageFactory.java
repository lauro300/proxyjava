/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.logic.message;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.urlProxy.Proxy;

/**
 *
 * @author jose_castaneda
 */
public class MessageFactory {
    public static Message getMessage(String head){
        if(Proxy.verbose){
            Logger.getLogger(MessageFactory.class.getName()).log(Level.INFO, "Head Message:{0}", head);
        }
        Message message = null ;
        String lowerCase = head.toLowerCase();
        if(lowerCase.startsWith("get")||lowerCase.startsWith("connect")){
            if(containsBrowser(head)){
                message = new MessageBrowser(head);
            } else {
                message = new GeneralMessage(head);
            }
        } else {
            if(head.startsWith("HTTP/")){
                message = new MessageServer(head);
            } else {
                return null;
            }
        }
        return message;
    }
    
    private static boolean containsBrowser(String head){
        for(String str: browsers) {
            if(head.contains(str))
                return true;
        }
        return false;
    }
    
    private static final String browsers[] ={"Firefox","Chrome","Spotify-Win32"};
}
