/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.logic.message;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.urlProxy.Proxy;
import static org.urlProxy.logic.protocol.AbstractProtocol.*;

/**
 *
 * @author jose_castaneda
 */
public class MessageBrowser implements Message{
    protected String rawMessage;
    protected StringBuilder tailMessage = new StringBuilder();
    protected Optional<String> url     = Optional.empty();
    protected Optional<String> host    = Optional.empty();
    protected Optional<Integer> port   = Optional.empty();
    protected Optional<TYPE_PROTOCOL> type = Optional.empty();
    protected Optional<String> command = Optional.empty();
    protected Optional<Integer> contentLength = Optional.empty();
    
    /**
         * Message expected: 
         * GET http://xataca.com/ HTTP/1.1
            Host: xataca.com
            User-Agent: Mozilla/5.0 (Windows NT 6.2; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0
            Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*\/*;q=0.8
            Accept-Language: es-MX,es-ES;q=0.8,es-AR;q=0.7,es;q=0.5,en-US;q=0.3,en;q=0.2
            Accept-Encoding: gzip, deflate
            Connection: keep-alive
     * @param head Raw petition from Browsers
         */
    protected MessageBrowser(String head) {
        init(head);
    }
    
    protected void init(String head){
        rawMessage = head;
        String lines[] = head.split("\n");
        Optional<String> tempURL = Optional.empty();
        for(String line:lines) {
            if(line.startsWith("GET")){
                type= Optional.of(TYPE_PROTOCOL.HTTP);
                command = Optional.of("GET");
                tempURL = Optional.of(line);
            } else {
                if(line.startsWith("Host")) {
                    String temp[] = line.split(":");
                    host = Optional.ofNullable(temp[1].trim());
                    type.filter(typeL->(typeL == TYPE_PROTOCOL.HTTP)&&(temp.length == 3))
                            .ifPresent(typeL->port=Optional.ofNullable(Integer.parseInt(temp[2].trim())));
                    type.filter(typeL->(typeL==TYPE_PROTOCOL.HTTPS) && (temp.length==3))
                            .ifPresent(typeL->port=Optional.ofNullable(Integer.parseInt(temp[2].trim())));
                } else {
                    if(line.startsWith("CONNECT")){
                        type = Optional.of(TYPE_PROTOCOL.HTTPS);
                        command = Optional.of("CONNECT");
                    } else {
                        if(line.toLowerCase().startsWith("content-length")) {
                            contentLength = Optional.of(Integer.parseInt(line.split(":")[1]));
                        } else {
                            tailMessage.append(line).append("\n");
                        }
                    }
                }
            }
        }
        tempURL.ifPresent((urlT)->urlFormat(urlT));
        if(Proxy.verbose){
            Logger.getLogger(MessageBrowser.class.getName()).log(Level.INFO, "Host:{0}, PORT:{1},URL:{2}", new Object[]{getHost(), getPort(), getURL()});
        }
        //System.out.println(getHost()+", PORT:"+getPort()+getURL());
    }
    
    @Override
    public String getRawMessage() {
        return rawMessage;
    }

    @Override
    public String getURL() {
        return url.orElse("");
    }

    @Override
    public TYPE_PROTOCOL getProtocol() {
        return type.orElse(TYPE_PROTOCOL.HTTP);
    }

    @Override
    public String getCommand() {
        return command.orElse("");
    }

    @Override
    public Integer getPort() {
        return port.orElse(80);
    }
    
    @Override
    public String getHost() {
        return host.orElse("");
    }
    
    @Override
    public String toString(){
        return new StringBuilder("URL:").append(url).append(",Host:").append(host)
                .append(",Port:").append(port).append(",Type:").append(type)
                .append(",Command:").append(command).toString();
    }

    private void urlFormat(String tempURL) {
        host.ifPresent(
                hostT->url = Optional.of(tempURL.replaceFirst(hostT, "").replaceFirst("[http]{4}://","")));
        if(port.filter(portl->(portl!=80) && (getProtocol()==TYPE_PROTOCOL.HTTP)).isPresent()){
            url = Optional.of(url.get().replaceFirst(":"+getPort(), ""));
        }
    }

    @Override
    public String getTailMessage() {
        return tailMessage.toString();
    }

    @Override
    public Integer getContentLength() {
        return contentLength.orElse(0);
    }
}
