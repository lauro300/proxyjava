/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.logic.message;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.urlProxy.Proxy;
import org.urlProxy.logic.protocol.AbstractProtocol;

/**
 *
 * @author jose_castaneda
 */
public class GeneralMessage extends MessageBrowser{
    
    protected GeneralMessage(String message){
        super(message);
    }
    
    @Override
    protected void init(String message){
        rawMessage = message;
        String lineTemp = null;
        String lines[] = message.split("\n");
        Optional<String> tempURL = Optional.empty();
        for(String line:lines) {
            if(line.startsWith("CONNECT")){
                type = Optional.of(AbstractProtocol.TYPE_PROTOCOL.HTTPS);
                command = Optional.of("CONNECT");
                lineTemp = line.replace("CONNECT ", "").replaceAll("HTTP/1.0", "").replaceAll("HTTP/1.1", "");
                String[] strHostPort = lineTemp.split(":");
                host = Optional.of(strHostPort[0]);
                port = Optional.of(Integer.parseInt(strHostPort[1].trim()));
            }
        }
        if(Proxy.verbose){
            Logger.getLogger(MessageBrowser.class.getName()).log(Level.INFO, "Host:{0}, PORT:{1}, URL:{2}", new Object[]{getHost(), getPort(), getURL()});
        }
    }
    
}
