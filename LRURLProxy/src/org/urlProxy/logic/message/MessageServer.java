/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.logic.message;

import java.util.Optional;
import org.urlProxy.logic.protocol.AbstractProtocol;

/**
 *
 * @author jose_castaneda
 */
public class MessageServer implements Message{
    private Optional<Integer> contentLength = Optional.empty();
    private long countBytes; 
    
    protected MessageServer(String head) {
        String lines[] = head.split("\n");
        for(String line:lines) {
            if(line.toLowerCase().startsWith("content-length")) {
                contentLength = Optional.of(Integer.parseInt(line.split(":")[1].trim()));
            }
        }
    }

    @Override
    public String getRawMessage() {
        return null;
    }

    @Override
    public String getTailMessage() {
        return null;
    }

    @Override
    public String getURL() {
        return null;
    }

    @Override
    public String getHost() {
        return null;
    }

    @Override
    public Integer getPort() {
        return null;
    }

    @Override
    public AbstractProtocol.TYPE_PROTOCOL getProtocol() {
        return null;
    }

    @Override
    public String getCommand() {
        return null;
    }

    @Override
    public Integer getContentLength() {
        return contentLength.orElse(0);
    }
    
    public void bytesReaded(long readedBytes){
        countBytes += readedBytes;
    }
    
    public long getBytesReaded(){
        return countBytes;
    }
}
