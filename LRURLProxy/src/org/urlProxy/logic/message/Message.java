/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.logic.message;

import static org.urlProxy.logic.protocol.AbstractProtocol.*;

/**
 *
 * @author jose_castaneda
 */
public interface Message {
    public String getRawMessage();
    public String getTailMessage();
    public String getURL();
    public String getHost();
    public Integer getPort();
    public TYPE_PROTOCOL getProtocol();
    public String getCommand();
    public Integer getContentLength(); 
}
