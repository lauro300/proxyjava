/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.urlProxy.logic.message.Message;
import org.urlProxy.logic.protocol.AbstractProtocol;
import org.urlProxy.logic.protocol.HTTPS;
import org.urlProxy.logic.protocol.HTTP;
import org.urlProxy.session.ReadHeadStrategy;
import org.urlProxy.session.Session;
import org.urlProxy.session.SessionManager;
import org.urlProxy.session.WriteRemoteHttpsStrategy;
import org.urlProxy.session.WriteRemoteStrategy;

/**
 *
 * @author jose_castaneda
 */
public class Proxy {
    public static final Boolean verbose = Boolean.valueOf(System.getProperty("verbose"));
    private static final Logger logger =  Logger.getLogger(Proxy.class.getName());
    public static final int SIZE_BUFFER = 1024;
    public static final CharsetDecoder decoder = StandardCharsets.UTF_8.newDecoder();
    private ServerSocketChannel server;
    private Selector selector;
    private final Map<SelectionKey, Long> timeOut = new HashMap<>();
    private Optional<NetworkInterface> net = Optional.empty();
    private Optional<NetworkInterface> netServer = Optional.empty();
    
    private AbstractProtocol protocols;
    Proxy(){
        try {
            initConfigServer();
            initProtocols();
            //net = NetworkInterface.getByName("wlan0");
            netServer = Optional.of(NetworkInterface.getByName("wlan0"));
            decoder.onMalformedInput(CodingErrorAction.IGNORE);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }
    
    private void initProtocols(){
        HTTP http = new HTTP();
        http.setNextProtocol(new HTTPS());
        protocols = http;
    }
    
    private void initConfigServer() throws IOException{
        server = ServerSocketChannel.open();
        netServer.ifPresent(netL->{
            try {
                server.socket().bind(new InetSocketAddress(netL.getInetAddresses().nextElement(),5050));
            } catch (IOException ex) {
                Logger.getLogger(Proxy.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        if(!netServer.isPresent()){
            server.socket().bind(new InetSocketAddress(5050));
        }
        server.configureBlocking(false);
        selector = Selector.open();
        server.register(selector, SelectionKey.OP_ACCEPT);
    }
    
    public void initProxy(){
        SelectionKey currentKey;
        while(true){
            try {
                selector.select();
                Iterator<SelectionKey> selectedKeys  = selector.selectedKeys().iterator();
                while(selectedKeys.hasNext()){
                    currentKey = selectedKeys.next();
                    selectedKeys.remove();
                    if(!currentKey.isValid()){
                        continue;
                    }
                    createSession(currentKey);
                }
            }catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    }
    
    protected void createSession(SelectionKey currentKey) throws IOException{
        if(!currentKey.isValid()){
            return;
        }
        if(currentKey.isAcceptable()){
            if(currentKey.channel() == server) {
                Session session = new Session();
                session.setClient(server.accept());
                session.getClient().configureBlocking(false);
                session.getClient().register(selector, SelectionKey.OP_READ);
                session.setStrategy(new ReadHeadStrategy());
                SessionManager.getInstance().addSession(session);
            }
        } else {
            if(currentKey.isReadable()){
                SessionManager
                        .getInstance()
                        .getSession((SocketChannel)currentKey.channel()).filter(sessionL->sessionL.isValid())
                        .ifPresent(Session::executeStrategy);
                SessionManager
                        .getInstance()
                        .getSession((SocketChannel)currentKey.channel())
                        .filter(sessionL->(!sessionL.getRemoteResource().isPresent()) && sessionL.isValid())
                        .ifPresent(sessionL->{
                    if(sessionL.getMessage().isPresent()){
                        try {
                            sessionL.getMessage().ifPresent(message->{
                                switch(message.getProtocol()){
                                    case HTTP:
                                        sessionL.setStrategy(new WriteRemoteStrategy());
                                        break;
                                    case HTTPS:
                                        sessionL.setStrategy(new WriteRemoteHttpsStrategy());
                                        break;
                                    default:
                                        break;
                                }
                            });
                            sessionL.setRemoteResource(Optional.of(SocketChannel.open()));
                            sessionL.getRemoteResource()
                                    .ifPresent(remote->{
                                try {
                                    remote.configureBlocking(false);
                                    net.ifPresent(netL->{
                                        try {
                                            remote.bind(new InetSocketAddress(netL.getInetAddresses().nextElement(),0));
                                        } catch (IOException ex) {
                                            Logger.getLogger(Proxy.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    });
                                    //remote.bind(new InetSocketAddress(net.getInetAddresses().nextElement(),0));
                                    remote.connect( 
                                            new InetSocketAddress(
                                                    sessionL.getMessage().map(Message::getHost).get(),
                                                    sessionL.getMessage().map(Message::getPort).get())
                                    );
                                    remote.finishConnect();
                                    remote.register(selector,SelectionKey.OP_CONNECT|SelectionKey.OP_READ);
                                    sessionL.getBuffer().clear();
                                    if(Proxy.verbose){
                                        Logger.getLogger(Proxy.class.getName()).log(Level.INFO, "Opening Sockect for: {0}",remote.toString());
                                    }
                                } catch(BindException ex){
                                    Logger.getLogger(Proxy.class.getName()).log(Level.WARNING, null, ex);
                                }catch (IOException ex) {
                                    Logger.getLogger(Proxy.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            });
                        } catch (IOException ex) {
                            logger.log(Level.SEVERE, null, ex);
                        }finally{
                            if(!SessionManager
                                    .getInstance()
                                    .getSession((SocketChannel)currentKey.channel())
                                    .filter(sessionT->
                                            sessionT.getMessage()
                                                    .map(Message::getProtocol)
                                                    .filter(protocolo->protocolo == AbstractProtocol.TYPE_PROTOCOL.HTTPS).isPresent()).isPresent()){
                                currentKey.cancel();
                            }
                        }
                    }
                });
                if(!SessionManager.getInstance().getSession((SocketChannel)currentKey.channel()).isPresent()){
                    currentKey.cancel();
                }
            } else {
                if(currentKey.isConnectable()){
                    if(((SocketChannel)currentKey.channel()).finishConnect()) {
                        SessionManager.getInstance()
                                .getSession((SocketChannel)currentKey.channel())
                                .filter(sessionL->sessionL.getRemoteResource().isPresent())
                                .ifPresent(sessionL->{sessionL.getRemoteResource()
                                        .ifPresent(remote->
                                                sessionL.getMessage()
                                                        .ifPresent(messageL->protocols.applicateProtocol(messageL, sessionL, sessionL.getBuffer())));
                                });
                    }
                } 
            }
        }
    }
    
    public static void main(String args[]) throws IOException {
        Proxy proxy = new Proxy();
        proxy.initProxy();
    }
}