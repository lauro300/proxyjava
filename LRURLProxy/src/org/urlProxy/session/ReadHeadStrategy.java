/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.session;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.urlProxy.logic.message.MessageFactory;

/**
 *
 * @author jose_castaneda
 */
public class ReadHeadStrategy implements Strategy{
    StringBuilder head = new StringBuilder();
    @Override
    public void doOperation(Session session) {
        try {
         String headL = readHead(session, session.getClient(), head, session.getBuffer());
         if(headL != null){
             session.message = Optional.ofNullable(MessageFactory.getMessage(head.toString()));
         }
        }catch(BufferUnderflowException ex){
            session.setValid(Boolean.FALSE);
            Logger.getLogger(ReadHeadStrategy.class.getName()).log(Level.WARNING, null, ex);
        }
    }
    
    protected String readHead(Session session, SocketChannel socket, StringBuilder head, ByteBuffer buffer){
        if(Strategy.readToStringBuilder(session,socket, buffer, "\n\r\n", head)){
            return head.toString();
        }
        return null;
    }
    
}
