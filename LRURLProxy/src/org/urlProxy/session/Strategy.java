/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.session;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.urlProxy.Proxy.decoder;

/**
 *
 * @author jose_castaneda
 */
public interface Strategy {
    public void doOperation(Session session);
    
    public static boolean readToStringBuilder(Session session, SocketChannel socket, ByteBuffer buffer, String limit, StringBuilder builder){
        int byteReaded;
        CharBuffer charBuffer;
        try {
            if(!socket.isConnected()){
                return false;
            }
            byteReaded = socket.read(buffer);
            if(byteReaded == -1){
                session.setValid(Boolean.FALSE);
            }
            buffer.flip();
            charBuffer = decoder.decode(buffer);
            while(charBuffer.position() != byteReaded) {
                if(!charBuffer.hasRemaining()){
                    break;
                }
                builder.append(charBuffer.get());
                if(builder.indexOf(limit)!=-1){
                    break;
                }
            }
            buffer.position(charBuffer.position());
            buffer.compact();
            if(builder.indexOf(limit)!=-1){
                return true;
            }
        } catch (IOException ex ) {
            session.setValid(Boolean.FALSE);
            Logger.getLogger(Strategy.class.getName()).log(Level.SEVERE, socket.toString(), ex);
        }
        return false;
    }
    
    public static int readSocketChannel(Session session,SocketChannel read, ByteBuffer buffer){
        if(!read.isOpen() && !read.isConnected()) {
            return -1;
        }
        int readedBytes = 0;
        try {
            readedBytes = read.read(buffer);
        } catch (IOException ex) {
            session.setValid(Boolean.FALSE);
            Logger.getLogger(Strategy.class.getName()).log(Level.SEVERE, read.toString(), ex);
        }
        return readedBytes;
    }
    
    public static void writeSocketChannel(Session session,SocketChannel write, ByteBuffer buffer){
        try {
            if(buffer == null||!write.isConnected()){
                return;
            }
            buffer.flip();
            write.write(buffer);
            buffer.compact();
        } catch (IOException ex) {
            session.setValid(Boolean.FALSE);
            Logger.getLogger(Strategy.class.getName()).log(Level.SEVERE, write.toString(), ex);
        }
    }
    
}
