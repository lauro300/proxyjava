/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.session;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jose_castaneda
 */
public class SessionManager {
    private final List<Session> sessions = new ArrayList();
    private static final SessionManager instance = new SessionManager();
    private SessionManager(){
    }
    
    public static SessionManager getInstance(){
        return instance;
    }
    
    public void addSession(Session session){
        sessions.add(session);
    }
    
    public Optional<Session> getSession(SocketChannel sc){
        eliminar();
        return sessions.stream().filter(session->{
            return (session.getClient() == sc || session.getRemoteResource().filter(remote->remote== sc).isPresent());
        }).findFirst();
    }
    
    public void eliminar(){
        sessions.stream().filter(sessionL->!sessionL.isValid()).forEach(sessionF->{
            try {
                sessionF.getClient().close();
                sessionF.getRemoteResource().ifPresent(socket->{
                    try {
                        socket.close();
                    } catch (IOException ex) {
                        Logger.getLogger(SessionManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sessionF.getBuffer().clear();
            }catch(IOException ex){
                Logger.getLogger(SessionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        sessions.removeIf(sessionL->!sessionL.isValid());
    }
    
    
}
