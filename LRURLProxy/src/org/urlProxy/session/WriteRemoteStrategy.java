/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.session;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.CharacterCodingException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.urlProxy.Proxy;
import static org.urlProxy.Proxy.decoder;
import org.urlProxy.logic.message.Message;
import org.urlProxy.logic.message.MessageFactory;

/**
 *
 * @author jose_castaneda
 */
public class WriteRemoteStrategy extends ReadHeadStrategy{
    protected Optional<Message> messageRemote = Optional.empty();
    protected ByteBuffer buffer = ByteBuffer.allocate(Proxy.SIZE_BUFFER);
    protected int byteCount = 0;
    @Override
    public void doOperation(Session session) {
        if(Proxy.verbose){
            Logger.getLogger(WriteRemoteStrategy.class.getName()).log(Level.INFO, "Reading Remote Socket for {0}",session.getRemoteResource());
        }
        try {
            messageRemote.ifPresent(message->writeSocket(session, message));
            if(!messageRemote.isPresent()) {
                session.getRemoteResource().ifPresent(socket->readSocket(socket, session));
            }
        }catch(BufferUnderflowException ex){
            session.setValid(Boolean.FALSE);
            Logger.getLogger(WriteRemoteStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected void readSocket(SocketChannel remote, Session session){
        int bytesReaded = Strategy.readSocketChannel(session, remote, buffer);
        if(Proxy.verbose){
            Logger.getLogger(WriteRemoteStrategy.class.getName()).log(Level.INFO,"Readed Bytes:{0}",bytesReaded);
        }
        if(bytesReaded == -1){
            session.setValid(Boolean.FALSE);
            return;
        } else {
            if(bytesReaded == 0 ){
                return;
            }
        }
        byte[] bufferL = new byte[Proxy.SIZE_BUFFER];
        int i = 0;
        buffer.flip();
        while(buffer.hasRemaining()) {
            bufferL[i++] = buffer.get();
        }
        Strategy.writeSocketChannel(session, session.getClient(), buffer);
        /*if(bytesReaded < Proxy.SIZE_BUFFER){
            session.setValid(Boolean.FALSE);
            return;
        }*/
        if(readHead(head, bufferL, bytesReaded)) {
            messageRemote = Optional.ofNullable(MessageFactory.getMessage(head.toString()));
            byteCount += (bytesReaded - head.length());
            if(Proxy.verbose){
                Logger.getLogger(WriteRemoteStrategy.class.getName()).log(Level.INFO,"Remote Readed Head:{0}",head.toString());
            }
        }
    }
    
    public boolean readHead(StringBuilder builder, byte[] bufferT, int byteReaded){
        try {
            CharBuffer charBuffer = decoder.decode(ByteBuffer.wrap(bufferT));
            while(charBuffer.position() != byteReaded) {
                builder.append(charBuffer.get());
                if(builder.indexOf("\n\r\n")!=-1){
                    break;
                }
            }
            if(builder.indexOf("\n\r\n")!=-1){
                return true;
            }
        } catch (CharacterCodingException ex) {
            Logger.getLogger(WriteRemoteStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    protected void writeSocket(Session session, Message messageR){
        session.getRemoteResource()
                .filter(remote->remote.isConnected())
                .ifPresent(remote->{
            int byteReaded = Strategy.readSocketChannel(session,remote, buffer);
            if(byteReaded == -1){
                session.setValid(Boolean.FALSE);
                return;
            } else {
                if(byteReaded == 0){
                    return;
                }
            }
            byteCount+=byteReaded;
            if(session.getClient().isConnected()){
                Strategy.writeSocketChannel(session,session.getClient(), buffer);
            }
            if(messageR.getContentLength() == byteCount) {
                session.setValid(Boolean.FALSE);
            }
        });
    }
    
}
