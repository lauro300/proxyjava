/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.session;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.urlProxy.Proxy;

/**
 *
 * @author jose_castaneda
 */
public class WriteRemoteHttpsStrategy extends WriteRemoteStrategy{
    protected ByteBuffer bufferRemote = ByteBuffer.allocate(Proxy.SIZE_BUFFER);
    @Override
    public void doOperation(Session session) {
        try {
            int clientReadB, remoteReadB;
            clientReadB = session.getClient().read(buffer);
            remoteReadB = session.getRemoteResource().get().read(bufferRemote);
            if(clientReadB == -1){
                session.setValid(Boolean.FALSE);
                return;
            }
            if(clientReadB == 0){
                bufferRemote.flip();
                session.getClient().write(bufferRemote);
                bufferRemote.clear();
            } else {
                if(clientReadB != 0){
                    buffer.flip();
                    session.getRemoteResource().get().write(buffer);
                    buffer.clear();
                }
            }
        }catch(BufferUnderflowException | IOException ex){
            session.setValid(Boolean.FALSE);
            Logger.getLogger(ReadHeadStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
