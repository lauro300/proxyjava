/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.session;

import java.nio.ByteBuffer;
/**
 *
 * @author jose_castaneda
 */
public class WriteStrategy implements Strategy {

    @Override
    public void doOperation(Session session) {
        session.getRemoteResource()
                .filter(remote->remote.isConnected())
                .ifPresent(remote->{
            ByteBuffer buffer = session.getBuffer();
            int byteReaded = Strategy.readSocketChannel(session,remote, buffer);
            if(byteReaded == -1 || byteReaded == 0){
                session.setValid(Boolean.FALSE);
                return;
            }
            if(session.getClient().isConnected()){
                Strategy.writeSocketChannel(session,session.getClient(), buffer);
            }
        });
    }
}
