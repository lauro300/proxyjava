/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.urlProxy.session;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Optional;
import static org.urlProxy.Proxy.SIZE_BUFFER;
import org.urlProxy.logic.message.Message;

/**
 *
 * @author jose_castaneda
 */
public class Session {
    private SocketChannel client;
    private Optional<SocketChannel> remoteResource = Optional.empty();
    private Strategy strategy;
    protected Optional<Message> message = Optional.empty();
    private ByteBuffer buffer;
    private Boolean valid = true;
    
    public void executeStrategy(){
        if(strategy == null){
            throw new RuntimeException("Strategy not set");
        }
        strategy.doOperation(this);
    }

    /**
     * @return the strategy
     */
    public Strategy getStrategy() {
        return strategy;
    }

    /**
     * @param strategy the strategy to set
     */
    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    /**
     * @return the client
     */
    public SocketChannel getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(SocketChannel client) {
        this.client = client;
    }

    /**
     * @return the remoteResource
     */
    public Optional<SocketChannel> getRemoteResource() {
        return remoteResource;
    }

    /**
     * @param remoteResource the remoteResource to set
     */
    public void setRemoteResource(Optional<SocketChannel> remoteResource) {
        this.remoteResource = remoteResource;
    }

    /**
     * @return the message
     */
    public Optional<Message> getMessage() {
        return message;
    }

    /**
     * @return the buffer
     */
    public ByteBuffer getBuffer() {
        return Optional.ofNullable(buffer).orElse(buffer=ByteBuffer.allocate(SIZE_BUFFER));
    }

    /**
     * @return the valid
     */
    public Boolean isValid() {
        return valid;
    }

    /**
     * @param valid the valid to set
     */
    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
