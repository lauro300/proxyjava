/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lauro
 */
public class HostTest {
    
    public HostTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getHost method, of class Host.
     */
    @Test
    public void testHostNamePort() {
        System.out.println("testHostNamePort");
        Host instance = new Host("www.operacion.com:8080");
        String expResult = "www.operacion.com";
        String result = instance.getHost();
        assertEquals(expResult, result);
        assertEquals(8080, instance.getPort().longValue());
    }
    
    @Test
    public void testIPNamePort(){
        System.out.println("testIPNamePort");
        Host instance = new Host("192.168.0.15:8080");
        String expResult = "192.168.0.15";
        String result = instance.getHost();
        assertEquals(expResult, result);
        assertEquals(8080, instance.getPort().longValue());
    }
    
    @Test
    public void testHostName() {
        System.out.println("testHostName");
        Host instance = new Host("www.operacionsinpuerto.com");
        String expResult = "www.operacionsinpuerto.com";
        String result = instance.getHost();
        assertEquals(expResult, result);
        assertEquals(80, instance.getPort().longValue());
    }
    
    @Test
    public void testIPName(){
        System.out.println("testIPName");
        Host instance = new Host("192.168.0.15");
        String expResult = "192.168.0.15";
        String result = instance.getHost();
        assertEquals(expResult, result);
        assertEquals(80, instance.getPort().longValue());
    }
    
    @Test
    public void testLocalHost(){
        System.out.println("testLocalHost");
        Host instance = new Host("testLocalHost");
        String expResult = "testLocalHost";
        String result = instance.getHost();
        assertEquals(expResult, result);
        assertEquals(80, instance.getPort().longValue());
    }
    
    @Test
    public void testLocalHostPort(){
        System.out.println("testLocalHostPort");
        Host instance = new Host("testLocalHost:8989");
        String expResult = "testLocalHost";
        String result = instance.getHost();
        assertEquals(expResult, result);
        assertEquals(8989, instance.getPort().longValue());
    }
    
    @Test
    public void testHostNameSubdomain(){
        System.out.println("testIPName");
        Host instance = new Host("subdomain.host.com.mx");
        String expResult = "subdomain.host.com.mx";
        String result = instance.getHost();
        assertEquals(expResult, result);
        assertEquals(80, instance.getPort().longValue());
    }
    
    @Test
    public void testHostNameLine(){
        System.out.println("testIPName");
        Host instance = new Host("sub-1doma-in.host.com.mx");
        String expResult = "sub-1doma-in.host.com.mx";
        String result = instance.getHost();
        assertEquals(expResult, result);
        assertEquals(80, instance.getPort().longValue());
    }
    
}
