/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jproxy;

import proxy.Proxy;
import proxy.interfaces.HttpsConnect;
import proxy.interfaces.SocketConfig;

/**
 *
 * @author lauro
 */
public class BuilderProxy {
    Proxy proxy = new Proxy();
    
    public BuilderProxy setPortB(Integer port) {
        proxy.setPort(port);
        return this;
    }
    
    public BuilderProxy setSocketCofigB(SocketConfig socketConfig){
        proxy.setSocketCofig(socketConfig);
        return this;
    }
    
    public BuilderProxy setHttpsConnectB(HttpsConnect httpsConnect){
        proxy.setHttpsConnect(httpsConnect);
        return this;
    }
    
    public Proxy getProxy(){
        return proxy;
    }
    
}
