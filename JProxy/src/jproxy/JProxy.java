/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jproxy;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import proxy.Host;
import proxy.OptionsProxy;
import proxy.Proxy;
import proxy.socketconfig.RedirectHostListProxy;
import proxy.socketconfig.RedirectHostProxy;

/**
 *
 * @author lauro
 */
public class JProxy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            BuilderProxy builderProxy = new BuilderProxy();
            parseParameters(args, builderProxy);
            Proxy proxy = builderProxy.getProxy();
            Thread proxyTh = new Thread(proxy);
            proxyTh.start();
            proxyTh.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(JProxy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void watchFileChanges(Path fileToWatch, RedirectHostListProxy listProxy){
        try {
            if(!Files.exists(fileToWatch, LinkOption.NOFOLLOW_LINKS)){
                return;
            }
            Path directory = fileToWatch.getParent();
            if(FileSystems.getDefault() == null) {
                System.out.println("System not supported for FileSystems.getDefault()");
                return;
            }
            final WatchService watch =  FileSystems.getDefault().newWatchService();
            directory.register(watch, StandardWatchEventKinds.ENTRY_MODIFY);
            new Thread(()->{
                while(true){
                    try {
                        final WatchKey wk = watch.take();
                        for(WatchEvent<?> event: wk.pollEvents()) {
                            if( (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) && 
                                    ((Path)event.context()).endsWith(fileToWatch.getFileName())
                               ) {
                                listProxy.setRedirectProxy(readFile(fileToWatch.toString()));
                                System.out.println("List updated");
                            }
                        }
                        if(!wk.reset()){
                            break;
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(JProxy.class.getName()).log(Level.SEVERE, null, ex);
                        break;
                    }
                }
            }).start();
        } catch (IOException ex) {
            Logger.getLogger(JProxy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void parseParameters(String[] parameters, BuilderProxy builder){
        Optional<List<String>> proxyList = Optional.empty();
        Optional<Integer> proxyPort = Optional.empty();
        Optional<Host> host = Optional.empty();
        String fileToRead = null;
        RedirectHostProxy hostProxy = null;
        for(String parameter:parameters) {
            if(parameter.contains(":")) {
                String values[] =parameter.split(":");
                OptionsProxy option = OptionsProxy.valueOf(parameter.toUpperCase().substring(0,parameter.indexOf(":")));
                switch(option){
                    case FILE:
                        proxyList = Optional.of(readFile(fileToRead=values[1]));
                        break;
                    case PORT:
                        proxyPort = Optional.of(Integer.parseInt(values[1]));
                        break;
                    case PROXY:
                        host = Optional.of(new Host(values[1],Integer.parseInt(values[2])));
                        break;
                    case VERBOSE:
                        Proxy.verbose = true;
                        break;
                }
            }
        }
        if(proxyList.isPresent() && host.isPresent()) {
                hostProxy = new RedirectHostListProxy(host.get().getHost(), host.get().getPort());
                ((RedirectHostListProxy)hostProxy).setRedirectProxy(proxyList.get());
                watchFileChanges(Paths.get(fileToRead),(RedirectHostListProxy)hostProxy);
            } else {
                if(!proxyList.isPresent() && host.isPresent()) {
                    hostProxy = new RedirectHostProxy(host.get().getHost(), host.get().getPort());
                }
            }
            if(hostProxy != null) {
                builder.setHttpsConnectB(hostProxy).setSocketCofigB(hostProxy);
            }
            if(proxyPort.isPresent()) {
                builder.setPortB(proxyPort.get());
            }
    }
    
    public static List<String> readFile(String file){
        Path path = Paths.get(file);
        if(Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
            try {
                return Files.readAllLines(path);
            } catch (IOException ex) {
                Logger.getLogger(JProxy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
}
