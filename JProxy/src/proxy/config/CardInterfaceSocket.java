/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy.config;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import proxy.interfaces.SocketConfig;

/**
 *
 * @author lauro
 */
public class CardInterfaceSocket implements SocketConfig {
    NetworkInterface  nit = null;
    public CardInterfaceSocket(String strInt){
        try {
            nit = NetworkInterface.getByName(strInt);
        } catch (SocketException ex) {
            ex.printStackTrace();
            Logger.getLogger(CardInterfaceSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Socket create(String host, int port) {
        try {
            Socket socket = new Socket();
            InetAddress itAdd= nit.getInetAddresses().nextElement();
            socket.bind(new InetSocketAddress(nit.getInetAddresses().nextElement(), 0));
            socket.connect(new InetSocketAddress(host,port));
            socket.setSoTimeout(DEFAULT_TIMEOUT);
            return socket;
        } catch (IOException ex) {
            Logger.getLogger(CardInterfaceSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static void main(String args[]) {
        try {
            Enumeration<NetworkInterface> nt = NetworkInterface.getNetworkInterfaces();
            while(nt.hasMoreElements()){
                NetworkInterface temp =  nt.nextElement();
                System.out.println(temp.getDisplayName()+":"+temp.getName());
            }
        } catch (SocketException ex) {
            Logger.getLogger(CardInterfaceSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
