/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author lauro
 */
public class Host {
    private static final Pattern pattern = Pattern.compile("(([A-Za-z0-9\\-]+\\.?)+)(:?)([0-9]*)?");
    private Optional<String> host;
    private Optional<Integer> port;
    
    public Host(String host){
        parseString(host);
    }
    
    public Host(String host, Integer port){
        this.host = Optional.of(host);
        this.port = Optional.of(port);
    }

    private void parseString(String host) {
        Matcher match =  pattern.matcher(host);
        if(match.find()){
            this.host = Optional.of(match.group(1));
            if((match.group(4)!=null) && (match.group(4).length()>0)){
                this.port = Optional.of(Integer.parseInt(match.group(4)));
            } else {
                this.port = Optional.empty();
            }
        }
    }
    
    public String getHost(){
        return host.orElse("www.google.com");
    }
    
    public Integer getPort(){
        return port.orElse(80);
    }
    
    public boolean isPresentPort(){
        return port.isPresent();
    }
    
    public static Host getHostHead(String head){
        String firtsLine = head.substring(0,head.indexOf("\n"));
        String hostLine = head.substring(head.indexOf("Host:"));
        hostLine = hostLine.substring(0,hostLine.indexOf("\n"));
        Host host;
        if(firtsLine.contains(":443") && !hostLine.contains(":443")){
            host = new Host(hostLine.replace("Host:", "").trim(),443);
        } else {
            host = new Host(hostLine.replace("Host:", "").trim()); 
        }
        if(Proxy.verbose){
            Logger.getLogger(HttpChannel.class.getName()).log(Level.INFO, "Host:"+host.getHost()+",Port:"+host.getPort()+"\nHead:"+head);
        }
        return host;
    }
    
}
