/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy.interfaces;

import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author lauro
 */
public interface SocketConfig {
    public int DEFAULT_TIMEOUT = 10 * 1000;
    public Socket create(String host, int port);
}
