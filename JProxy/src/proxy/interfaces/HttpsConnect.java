/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy.interfaces;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 *
 * @author lauro
 */
public interface HttpsConnect {
    public void createTunnelThreadHttps(String head,Socket channel, InputStream inputCl, OutputStream outputCl);
}
