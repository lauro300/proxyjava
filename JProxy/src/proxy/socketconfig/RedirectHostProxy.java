/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy.socketconfig;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import proxy.Host;
import proxy.HttpChannel;
import proxy.interfaces.HttpsConnect;
import proxy.interfaces.SocketConfig;

/**
 *
 * @author lauro
 */
public class RedirectHostProxy implements SocketConfig, HttpsConnect{
    private Optional<Host> proxyhost;
    
    public RedirectHostProxy(String host, Integer port){
        proxyhost = Optional.of(new Host(host, port));
    }
    
    @Override
    public Socket create(String host, int port){
        Socket socket=null;
        try{
            socket = new Socket(proxyhost.get().getHost(), proxyhost.get().getPort());
            socket.setSoTimeout(SocketConfig.DEFAULT_TIMEOUT);
            return socket;
        }catch(IOException io){
            Logger.getLogger(RedirectHostProxy.class.getName()).log(Level.SEVERE, null, io);
        }
        return null;
    }

    @Override
    public void createTunnelThreadHttps(String head, Socket channel, InputStream inputCl, OutputStream outputCl) {
        try {
            channel.getOutputStream().write(head.getBytes());
            channel.getOutputStream().write("\r\n".getBytes());
            channel.getOutputStream().flush();
        } catch (IOException ex) {
            Logger.getLogger(RedirectHostProxy.class.getName()).log(Level.SEVERE, null, ex);
        }
        Thread read = new Thread(()->{
            try(BufferedOutputStream bos = new BufferedOutputStream(channel.getOutputStream())){
                int dato;
                byte []buffer = new byte[4096];
                while((dato = inputCl.read(buffer))!= -1) {
                    bos.write(buffer, 0, dato);
                    bos.flush();
                }
            }catch(IOException ioe) {}
            finally{
                try {
                    inputCl.close();
                    outputCl.close();
                }catch(Exception ex){}
            }
        });
        Thread write = new Thread(()->{
            try(BufferedInputStream bis = new BufferedInputStream(channel.getInputStream())){
                int dato;
                byte []buffer = new byte[4096];
                while((dato = bis.read(buffer))!= -1){
                    outputCl.write(buffer,0,dato);
                    outputCl.flush();
                }
            }catch(IOException ex) {}
            finally{
                try {
                    inputCl.close();
                    outputCl.close();
                }catch(Exception ex){}
            }
        });
        read.start();
        write.start();
        try {
            read.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(HttpChannel.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            try {
                if(!channel.isClosed()) {
                    channel.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(RedirectHostProxy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public Host getProxyhost(){
        return proxyhost.get();
    }
    
}
