/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy.socketconfig;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import proxy.HttpChannel;
import proxy.Proxy;
import proxy.interfaces.HttpsConnect;
import proxy.interfaces.SocketConfig;

/**
 *
 * @author lauro
 */
public class RedirectHostListProxy extends RedirectHostProxy{
    private List<String> redirectProxy = new LinkedList<>();
    
    public RedirectHostListProxy(String host, Integer port) {
        super(host, port);
    }
    
    @Override
    public Socket create(String host, int port) {
        if(getRedirectProxy().contains(host)){
            return super.create(host, port);
        } else {
            try {
                Socket socket = new Socket(host, port);
                socket.setSoTimeout(SocketConfig.DEFAULT_TIMEOUT);
                return socket;
            } catch (IOException ex) {
                Logger.getLogger(RedirectHostListProxy.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
    }

    /**
     * @return the redirectProxy
     */
    public List<String> getRedirectProxy() {
        return redirectProxy;
    }

    /**
     * @param redirectProxy the redirectProxy to set
     */
    public void setRedirectProxy(List<String> redirectProxy) {
        if(!this.redirectProxy.isEmpty()) {
            this.redirectProxy.clear();
        }
        this.redirectProxy.addAll(redirectProxy);
    }
    
    @Override
    public void createTunnelThreadHttps(String head, Socket channel, InputStream inputCl, OutputStream outputCl) {
        if(redirectProxy.contains(channel.getInetAddress().getHostName())|| getProxyhost().getHost().equals(channel.getInetAddress().getHostName())|| channel.getInetAddress().getHostName().equals("127.0.0.1")) {
            if(Proxy.verbose) {
                Logger.getLogger(HttpChannel.class.getName()).log(Level.INFO, "Redirect:"+channel);
            }
            super.createTunnelThreadHttps(head, channel, inputCl, outputCl);
        } else {
            new HttpsConnect() {
                @Override
                public void createTunnelThreadHttps(String head, Socket channel, InputStream inputCl, OutputStream outputCl) {
                    try {
                        outputCl.write("HTTP/1.0 200 Connection established\r\nProxy-agent: RPS-Proxy/1.0".getBytes());
                        outputCl.write("\r\n\r\n".getBytes());
                        outputCl.flush();
                    } catch (IOException ex) {
                        Logger.getLogger(HttpChannel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    Thread read = new Thread(()->{
                        try(
                                BufferedOutputStream bos = new BufferedOutputStream(channel.getOutputStream());
                            ){
                            int dato;
                            byte []buffer = new byte[4096];
                            while(!channel.isClosed() &&  ((dato = inputCl.read(buffer))!= -1)) {
                                bos.write(buffer, 0, dato);
                                bos.flush();
                            }
                        }catch(IOException ioe) {}
                        finally{
                            try {
                                inputCl.close();
                                outputCl.close();
                            }catch(Exception ex){}
                        }
                    });
                    Thread write = new Thread(()->{
                        try(BufferedInputStream bis = new BufferedInputStream(channel.getInputStream())){
                            int dato;
                            byte []buffer = new byte[4096];
                            while(!channel.isClosed() &&((dato = bis.read(buffer))!= -1)){
                                outputCl.write(buffer,0,dato);
                                outputCl.flush();
                            }
                        }catch(IOException ex) {}
                        finally{
                            try {
                                inputCl.close();
                                outputCl.close();
                            }catch(Exception ex){}
                        }
                    });
                    read.start();
                    write.start();
                    try {
                        read.join();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(HttpChannel.class.getName()).log(Level.SEVERE, null, ex);
                    }finally{
                        try {
                            if(!channel.isClosed()) {
                                channel.close();
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(RedirectHostProxy.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }.createTunnelThreadHttps(head, channel, inputCl, outputCl);
        }
    }
}
