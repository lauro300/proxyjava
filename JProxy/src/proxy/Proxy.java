package proxy;

import proxy.interfaces.SocketConfig;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import proxy.interfaces.HttpsConnect;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lauro
 */
public class Proxy implements Runnable{
    private Optional<Integer> port;
    private SocketConfig socketCofig;
    private HttpsConnect httpsConnect;
    public static boolean verbose = false;
    
    public Proxy(){
        this(null);
    }
    
    public Proxy(Integer port){
        this(port, null);
    }
    
    public Proxy(Integer port, SocketConfig socketConfig){
        this.port = Optional.ofNullable(port);
        this.socketCofig = socketConfig;
    }
    
    public void run(){
        try(ServerSocket server = new ServerSocket(getPort());) {
            ThreadFactory factory = Executors.defaultThreadFactory();
            ExecutorService thPool = Executors.newCachedThreadPool();
            while(true) {
                thPool.execute(new HttpChannel(server.accept(),socketCofig,httpsConnect));
            }
        } catch (IOException ex) {
            Logger.getLogger(Proxy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the port
     */
    public int getPort() {
        return port.orElse(5050);
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        this.port = Optional.ofNullable(port);
    }

    /**
     * @return the socketCofig
     */
    public SocketConfig getSocketCofig() {
        return socketCofig;
    }

    /**
     * @param socketCofig the socketCofig to set
     */
    public void setSocketCofig(SocketConfig socketCofig) {
        this.socketCofig = socketCofig;
    }
    
    public void setHttpsConnect(HttpsConnect htppsConnect) {
        this.httpsConnect = htppsConnect;
    }
    
}
