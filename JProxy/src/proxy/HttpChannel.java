/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import proxy.interfaces.HttpsConnect;
import proxy.interfaces.SocketConfig;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lauro
 */
public class HttpChannel implements Runnable, HttpsConnect{
    public static String CONTENT_LENGTH = "content-length";
    private Optional<Socket> client;
    private Host host;
    private Optional<SocketConfig> socketConfig;
    private Optional<HttpsConnect> httpsConnection;
    public HttpChannel(Socket client){
        this(client, null);
    }
    
    public HttpChannel(Socket client, SocketConfig socketConfig){
        this(client, socketConfig, null);
    }
    
    public HttpChannel(Socket client, SocketConfig socketConfig, HttpsConnect httpsConnection) {
        this.client= Optional.of(client);
        this.socketConfig = Optional.ofNullable(socketConfig);
        this.httpsConnection = Optional.ofNullable(httpsConnection);
    }
    
    
    private void writeBlock(OutputStream out, byte bytes[]) throws IOException {
        out.write(bytes);
        out.write("\r\n\r\n".getBytes());
        out.flush();
    }
    
    private String readStringBlock(InputStream in) throws IOException{
        int data;
        StringBuilder strBuffer = new StringBuilder();
        String ln;
        while((ln = readLine(in))!=null){
            strBuffer.append(ln);
        }
        return strBuffer.toString();
    }
    
    private String readLine(InputStream in) throws IOException{
        int data;
        StringBuilder builder = new StringBuilder();
        while((data=in.read())!=-1){
            builder.append((char)data);
            if(data=='\n'){
                break;
            }
        }
        if(builder.toString().equals("\r\n")){
            return null;
        }
        return builder.toString();
    }
    
    @Override
    public void run(){
        try(BufferedInputStream inputCl = new BufferedInputStream(client.get().getInputStream());
            BufferedOutputStream outputCl = new BufferedOutputStream(client.get().getOutputStream());) {
            String head = readStringBlock(inputCl);
            setHost(head);
            try(Socket channel = openConnection();){
                if(channel == null) {
                    return;
                }
                if(isHttps(head)) {
                    createTunnelThreadHttps(head,channel,inputCl,outputCl);
                } else {
                    createTunnel(channel, head,inputCl,outputCl);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(HttpChannel.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            try{
                if(!client.get().isClosed()){
                    client.get().close();
                }
            }catch(IOException ex) {
                Logger.getLogger(HttpChannel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private boolean isHttps(String request) {
        return request.startsWith("CONNECT");
    }

    /*private void createTunnelThread(Socket channel, final InputStream input, final OutputStream output) throws IOException {
        
    }*/

    private Socket openConnection() throws IOException {
        return socketConfig.orElse((String hostSC, int portSC)->{
            try{
                Socket socket = new Socket(hostSC, portSC);
                socket.setSoTimeout(SocketConfig.DEFAULT_TIMEOUT);
                return socket;
            }catch(IOException ex){
                Logger.getLogger(HttpChannel.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }).create(host.getHost(), host.getPort());
    }

    private void setHost(String head) {
        host = Host.getHostHead(head);
    }

    private void createTunnel(Socket channel, String head, InputStream input, OutputStream output) {
        try(
                BufferedOutputStream outCh = new BufferedOutputStream(channel.getOutputStream());
                BufferedInputStream   inCh = new BufferedInputStream(channel.getInputStream());
                ){
            StringBuilder temp = new StringBuilder();
            temp.append(head);
            String request;
            if(!head.startsWith("GET /")) {
                request = temp.toString().replaceFirst("[http]{4}://","").replaceFirst(host.getHost(), "");
            } else {
                request = head;
            }
            if(host.isPresentPort()){
                request = request.replaceFirst(":"+host.getPort(), "");
            }
            if(Proxy.verbose){
                Logger.getLogger(HttpChannel.class.getName()).log(Level.INFO, "Host:"+host.getHost()+",Port:"+host.getPort()+"\nRequest:"+request);
            }
            writeBlock(outCh, request.getBytes());//requestLine.getBytes());
            String headCh = readStringBlock(inCh);
            int indexCL = headCh.toLowerCase().indexOf(CONTENT_LENGTH.toLowerCase());
            int dataLength = -1;
            if(indexCL != -1) {
                String contentLength = headCh.substring(indexCL);
                contentLength = contentLength.substring(0, contentLength.indexOf("\n")).toLowerCase().replace(CONTENT_LENGTH+":", "");
                dataLength = Integer.parseInt(contentLength.trim());
            } 
            output.write(headCh.getBytes());
            output.write("\r\n".getBytes());
            output.flush();
            int dataReaded = 0;
            byte []buffer = new byte[4096];
            int byteR;
            while(!channel.isClosed() && ((byteR = inCh.read(buffer)) != -1)){
                output.write(buffer,0,byteR);
                dataReaded+=byteR;
                if(dataLength != -1 &&(dataReaded == dataLength)) {
                    break;
                } else {
                    if((dataLength == -1) && new String(buffer,0,byteR).endsWith("\r\n\r\n")){
                        break;
                    }
                }
            }
            output.flush();
        }catch(SocketTimeoutException ex){}
         catch(IOException ex){
            ex.printStackTrace();
        } finally {
            try {
                if(!channel.isClosed()) {
                    channel.close();
                }
                input.close();
                output.close();
            } catch (IOException ex) {
                Logger.getLogger(HttpChannel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    /**
     * @param socketConfig the socketConfig to set
     */
    public void setSocketConfig(SocketConfig socketConfig) {
        this.socketConfig = Optional.ofNullable(socketConfig);
    }

    @Override
    public void createTunnelThreadHttps(String head, Socket channel, InputStream inputCl, OutputStream outputCl) {
        httpsConnection.orElse((String headL, Socket channelL, InputStream inputClL, OutputStream outputClL)->{
            try {
                writeBlock(outputCl,"HTTP/1.0 200 Connection established\r\nProxy-agent: RPS-Proxy/1.0".getBytes());
            } catch (IOException ex) {
                Logger.getLogger(HttpChannel.class.getName()).log(Level.SEVERE, null, ex);
            }
            Thread read = new Thread(()->{
                try(BufferedOutputStream bos = new BufferedOutputStream(channel.getOutputStream())){
                    int dato;
                    byte []buffer = new byte[4096];
                    while((!channel.isClosed())&&((dato = inputClL.read(buffer))!= -1)) {
                        bos.write(buffer, 0, dato);
                        bos.flush();
                    }
                }catch(IOException ioe) {}
            });
            Thread write = new Thread(()->{
                try(BufferedInputStream bis = new BufferedInputStream(channel.getInputStream())){
                    int dato;
                    byte []buffer = new byte[4096];
                    while((!channel.isClosed())&&((dato = bis.read(buffer))!= -1)){
                        outputClL.write(buffer,0,dato);
                        outputClL.flush();
                    }
                }catch(IOException ex) {}
            });
            read.start();
            write.start();
            try {
                read.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(HttpChannel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).createTunnelThreadHttps(head, channel, inputCl, outputCl);
    }
    
    public void setHttpsConnection(HttpsConnect httpsConnect ){
        this.httpsConnection = Optional.ofNullable(httpsConnect);
    }
    
}
